## Conceptual Architecture

[Presentation Slides](https://drive.google.com/file/d/1O9GcdvlKDMW6LVtCQrsbed8E4hclj1ri/view?usp=sharing)

[Report (PDF)](https://drive.google.com/file/d/1dR_G0xWJNm-HCpy05jA_F3U4hTSG_Swb/view?usp=sharing)


Abstract
========

The report discusses about the conceptual architecture of the IntelliJ IDEA system. The report discusses about the functionality and the architecture of the main and the sub components of the system. State and Use case diagrams are used to show the interaction of the user and the software system.  

Introduction
============

System Functionality
--------------------

IntelliJ IDEA is an extensible and fully featured IDE for JVM based languages such as Java, Scala, Groovy and Clojure that is available for MacOS, Linux and Windows operating system. The community version of IntelliJ is free to use and open source under Apache 2 License. Like all other JetBrains products, IntelliJ IDEA IDE is also built on IntelliJ Platform.

The IntelliJ Platform provides all of the infrastructure that IDEs need to provide rich language tooling support. It provides:

*   a component driven, cross-platform JVM based application host
*   high-level user interface toolkit
*   text editor with syntax highlighting, code folding, code completion
*   image editor
*   open APIs to build common IDE functionality
*   language agnostic infrastructure for parsing and indexing code

### Holistic look at major components

IntellIJ IDEA is based entirely on IntellIJ Platform. IntelliJ Platform comprises of the following major subsystems: Core, Application, UI, PSI, Project Model, VFS, Plugin. IntelliJ is built on arrays of components that communicate with each other and pass data and control. Thus, every component needs the Core as it provides dependency injection (to create and destroy objects at runtime), concurrency support (threading) and communication. VFS creates snapshots of file system which is used by PSI and Project Model. Application encapsulates other subsystems and provides plugins with a straightforward API.

![](a1-assets/images/image6.jpg)  
                        Figure 1: IntelliJ Conceptual Architecture  

Subsystems
==========

Core
----

IntelliJ Platform core includes:

1.  Component Model
2.  Threading and Concurrency
3.  Messaging
4.  Disposers

### Component Model

The Component Model is designed to create loosely coupled, composable applications. When IntelliJ is launched, firstly, the Component Model will look for classes marked with specific attributes, which declare the lifetime scope of the component. This information is also described using Facets. Dependencies are declared as constructor arguments. At the appropriate time, the Component Model will create new instances of the components, ensuring all dependencies are created first, and passed into the constructor. This provides the ability to live load and unload new components, during runtime. Component Model is synonymous to an Inversion of Control[\[1\]](#ftnt1) framework.

The Component Model uses a Dependency Injection framework called PicoContainer to instantiate arbitrary objects. Each PicoContainer[\[2\]](#ftnt2) has a lifecycle; the container will figure out the correct order of invocation of start()/stop() all the objects managed by the container. There is a hierarchy of PicoContainer containers. Registration of components is usually done by configuration found in XML files that define individual containers.[\[3\]](#ftnt3)

### Threading & Concurrency

The Threading system provided by IntelliJ is designed to provide the system a single reader/writer lock. This system is a stand alone module that services the entire system. It provides a system that allows for asynchronous code execution as well as other special cases. The threading system provides developers special use cases such as invokeLater which allows a thread the ability to wait until other processes have concluded. All threads can be named and logged within the IntelliJ’s logging system. It also allows for read action cancellability which allows a thread to be suspended when a new action begins or until another action has concluded. Read action cancellability is most prominently shown when a user beginning typing into the interface, without it the user would have to wait until all other processes have concluded before their text appears on the screen.

The threading system is quite obviously based off of the Readers–writer lock pattern. This pattern allows for concurrent access for readers but only allows for exclusive access for writers. This means that several readers can be accessing the same data but only one writer can be changing data at a time. This protocol is useful for ensuring data without any anomalies. This pattern is normally built using mutexes and condition variables.

### Messaging

The messaging system is used to provide a publisher subscription model and allowing developers to quickly implement a more robust observer pattern. The messaging system is a stand alone module that is used by other modules for communication purposes. Models can create observers by instantiating a Topic class. Other models can subscribe to this topic class creating an observer/subscriber relationship. The messaging system provides the developer all the tools required to create an observer/subscriber pattern as well as other special cases for the intelliJ system. IntelliJ’s messaging system provides a feature called broadcasting which allows subscribers subscribed to other topics to receive notifications broadcast from observers they are not subscribed to. The system also provides nested event handlers, this allows for subscribed events to fire while another subscribed event is being processed, both events are guaranteed to be handled in the order that they were sent.

The messaging infrastructure is based on the Publish-subscribe pattern. It uses a messaging pattern wherein the publishers send out messages without specifically knowing what the messages are for. The subscribers are responsible for listening for the messages and acting accordingly to process and categorize the message. The use of this pattern allows the IntelliJ platform to scale easily as it provides flexibility to other features using the messaging system.

### Disposers

IntelliJ Platform also supports Services which are processes that run on background threads irrespective of component model manager. In order to kill these processes gracefully, the platform offers a simple API via disposers.Example: shut down a daemon and all its associated processes

Plugin
------

The only possible way to use third parties to extend IDEA functionality is plugins. The structure of plugins includes plugin content, plugin class loaders, plugin components, plugin services, plugin extensions, extension points and plugin actions.

Plugin content contains plugin icon file, plugin configure file and classes which implement the plugin functionality. Each plugin has one plugin class loader which is used to record those classes. Plugin components are the fundamental of plugin integration, and they have three types including application level component, project level component and module level component. Plugin service is optional and part of plugin component, but clients usually are encouraged to use plugin service for better performance. What is more, plugin services also have three different levels to correspond to plugin components. IntelliJ provides the concept of extension and extension point for plugins interaction. Extension points are used to allow other plugins to extend its functionality, and extensions are used to extend functionality. Plugin Action allows plugin to add their own item to IDEA menus and toolbars.

Application and Project Model
-----------------------------

The application provides access to the core functionalities and threading model of the IDE. Plugins created on the application level will be initialized with the start up of the IDE.

The project model consists of the source code, libraries, software development kits, modules and facets. Each project can consist of one or more modules which are units of runnable, testable code. Both libraries and SDKs can be set to work for the project level or module level. There is the additional option to have global libraries, so that it is visible to many projects. Lastly there are facets, which are framework configurations for modules. A module can have more than one facet.

UI
--

User interface allows the user to interact with the software system to perform specific tasks. There are several features the UI provides to the user in assisting them to carry out their task, for eg: Tool Window, Popup Windows, Dialog Wrapper, etc.

The user interface has a huge dependency on the action component of the system. In the user interface there are many buttons and each button has an action listener associated with it. The task of this action listener is to listen when the button is pressed and then run the appropriate functionality. All the action listener and their respective functionality after they are pressed is defined in the action component. That is why the UI relies heavily on the action component to function.

In the UI the tool windows are like child windows of the main window. They are relying on different components of the system. For instance the debugger tool window relies on the debugger component of the system to provide it with information to be displayed. Also the run tool window displays the result of the code being compiled and as such needs the result from the compiler component. There is also the file tool window which relies on VFS to provide it with the updated snapshot of the file system to be displayed. The tool window in the UI uses different architecture to connect to these components. For eg: The run tool window uses pipe and filter (batch sequential) architecture style.

### Editor

The editor window  allows the user to add text to the file. The editor can have different tabs for different files. The editor window has various features to help the user such as syntax error checking, grammatical mistakes, auto completion of the code, etc.

The system has listener processes running in the background. These processes are listening for any changes. The editor makes use of these processes as well. The editor uses the keystroke listener. Whenever something is pressed on the keyboard that event is listened and the output (ASCII Characters) is displayed on the screen. The registering and listening for keystrokes is defined in the action component of the system and thus the editor depends on the action module as well.

The editor also provides features described above like syntax error checking. The editor does this using the publisher subscriber design pattern. Whenever an event happens, for example the user makes syntax error while typing the event is published that a syntax error had taken place. The methods that have subscribed to this event then start running and in doing so provides an error message to the user as well as suggestions on how to fix the error.

### Action System

The action class deals with the user interaction with the software. It deals with events and the system UI. This class also let plugin to add items to menus and toolbars. An action includes hotkeys, UI behavior like buttons and menus and toolbars, shortcut, text related event like cut copy etc., most of the user interaction with the IDEA can be defined in the action class. The actions system allows plugins to define and add in their own actions and behavior as well as add to the menu and toolbars.

Actions are groupable, meaning you can group multiple actions and those groups can contain other groups. IDEA also allows same events in different groups. To help identify the actions, every action and action group required a unique identifier.

There are similarities between the action system architecture and the Process-Control architecture. In this case the Actions defined the user behaviors and acts like a controller calling the specific parts to react to it. In this case, the process can either be the UI or other subsystems that are fit for the job.

Virtual File System
-------------------

The IntelliJ Platform maintains a Virtual File System (VFS) component responsible for working with files. Essentially, VFS provides an abstract layer over different file systems. This has the added benefit of implementing a file system, such as FTP, and not having to  worry about file location, storage and other low level operations. Figure 2 provides a visual representation of thisabstraction.

![](a1-assets/images/image5.jpg)

Figure 2: VFS is an abstract layer for working with files regardless of their location

At its core, VFS provides a universal API for working with files, the ability to track file modifications and associate additional persistent data with a file. This is done by maintaining presistenet snapshots of the files in the local disk. Snapshots have an application level scope meaning that they multiple projects may refer to the same file instance in VFS. These snapshots are updated asynchronously via a refresh operation which uses the threading component. Hence, the VFS component has a dependency on the threading component through the application module. VFS uses the observer patternto notify other components in case of changes/updates made to the snapshot via VFS events. The observer pattern being described here closely resembles the messaging component. One use case of this is the IntelliJ Platform UI which displays file state based on the snapshots. Therefore, if a snapshot is updated, UI has to reflect this change (vice-versa), so a VFS event is triggered in the event-dispatch thread. Figure 3 describes this behaviour in detail using a scenario when the user changes a file in editor.

![](a1-assets/images/image3.jpg)

Figure 3: Behaviour of components when a file is changed by user in UI

A file in the VFS can be created in one of two ways: using PSI API or Java file API. We will focus on the former because it relates to the overall conceptual architecture. A file in the VFS has a tree-structure which is why PSI can reference or create an instance of virtual file via the Project and action component. The next section will discuss how this is achieved.

PSI
---

The Program Structure Interface (PSI) manages the builds for the syntactic and semantic code model of many file types. The PSI structures itself as a tree, where the root is a PSI file and the nodes of the tree are PSI elements. An example of how this tree could look is shown in Figure 4. With the PSI tree, a user can navigate and modify PSI elements.

![](a1-assets/images/image2.png)

Figure 4: The PSI Tree can be organized in many ways with the PSI File as the root

The PSIFile class is a parent class to language specific PSI file classes. For example, there is the PSIJavaFile class and the XMLFile class. If a file uses multiple languages a File View Provider can be used to manage multiple PSI trees for each language used in the file. For creating PSI files, the PSI uses the abstract factory design pattern to allow configuration of the file contents, providing control over factors such as language, file attributes and more.From a PSI file, an iterator over its child elements can be obtained.

With a PSI element, source code can be explored like it is interpreted by the IntelliJ platform and modified. Examples include using the refactoring feature to rename a PSI element. Language specific operations can also be performed such as finding the superclass of a Java class.

The PSI lives in the context of a project. In order to access files through the virtual file system, the PSI goes through an instance of its project. Then PSI files opened in another project are separate instances. PSI files or elements can be obtained through  actions and used in code inspections.

Architectural Styles
====================

Layered Architecture

Plugin components can be a layered architecture style. There are 3 layers, Application level layer, Project level layer and Module level layer. Application level layer is the top-most layer in these layers. What is more, the layers only interact with the layers below so the lower level layers will not interrupt with the higher level layers.

Application level layer of plugin components can be created and initialized when IntelliJ IDEA start up. They usually have no dependencies, and their constructor have no parameters. However, if needed, an application component can be used as parameter of another application component. Project level layer components has the similar structure of application level components, but they use application level components and project level components as parameters in their constructors. Module level layer is the lowest layer so module level components can specify other components as parameters.

Pipe and Filter (Batch-Sequential) Architecture

The pipe and filter architecture is used in the run tool window of the UI. The run tool window shows the runtime result of the compiled code. The code is compiled in the compiler component of the system and the result is then sent to the run tool window in the UI.

Since the result has to first be finalised before being displayed the pipe and filter used is batch sequential.

Evolution of IntelliJ Platform
==============================

The first version of IntelliJ IDEA was published in January 2001, which was created by JetBrains.The primary purpose of  IntelliJ was to be a cross-platform with a focus on Java and website development. Hence, IntelliJ IDEA was one of the most popular Java programming tools. By 2009, IntelliJ IDEA become an open-source IDE. Other development environments were created and based on IntelliJ’s framework. such as PHPStorm, WebStorm and PyCharm.  

Use cases

![](a1-assets/images/image8.png)

Figure 5: A use case for module level plugin component lifecycle

Module level Plugin components are created for each Module inside every project loaded in the IDE. Module instance can acquire module level components with getComponent() method. When module level components are acquired, their constructor will be invoked first. Then, module level components will be initialized by initComponent() of BaseComponent Interface which is the base interface class for all components. If module level component implements the PersistentStateComponent interface, it can call loadState() to get its state. Finally, module plugin component will call moduleAdded() to notify that a module has been added to a project.

Responsibilities among participating developers
===============================================

About the Open Source project
-----------------------------

We used GitStats[\[4\]](#ftnt4) to generate the following stats on IntelliJ IDEA Community Edition code repository on GitHub[\[5\]](#ftnt5).

### Commits Activity

![](a1-assets/images/image1.png)

### Lines of Code

![](a1-assets/images/image4.png)

### Total Files

![](a1-assets/images/image7.png)

Figure 6:  Statistics on IntelliJ’s repository

Derivation Process
==================

Starting with the official documentation of IntelliJ Platform, we were able to deduce some of the components involved at high-level. However, it was hard to justify how these components interacted with each other due to lack of documentation and references. This lead us to the study the eclipse architecture. Using eclipse as a reference guide, we were able to fill in some of the missing blanks.[\[6\]](#ftnt6)

Alternatives to IntelliJ
========================

Eclipse and Netbeans are the top competitors of IntelliJ IDEA. Although using a particular IDE is matter of preference, IntelliJ is better for building code-centric tools and framework or other IDE’s[\[7\]](#ftnt7). However, one main disadvantage that IntelliJ has is that it's not a  general-purpose rich client platform (RCP). In addition, Eclipse and NetBeans are generally a better IDE for building graphic intensive applications. A user looking to choose between these IDE’s, may want to consider the type of application the client is asking for before choosing.

Smart completion

Chain completion

Static members completion

Data flow analysis

Language injection

Cross-language refactorings

Polyglot experience (PSI)

Developer ergonomics

Editor-centric environment

Shortcuts for everything

Ergonomic user interface

Inline debugger

Version control (AST tree)

Terminal

Detecting duplicates

Inspections and quick-fixes

Build tools

Test runner and coverage

Decompiler

Database tools

Application servers

Docker

Figure 7: IntelliJ Notable Features

Conclusion
==========

IntelliJ is an open source platform built to provide tools for developers to build software applications. Large scale applications such as IntelliJ IDEA have a lot of inter-connected components therefore, it is crucial to have a conceptual architecture that serves as a big picture. Developers and new-comers are able to refer to this architecture and gain intuition into how the overall system functions and behaves. We were able to identify the use of Layered architecture in Plugins component, pipe-filter (batch) in UI, and the abstract factory pattern in PSI. This lead us to develop our own hybrid conceptual architecture found in Figure 1.

Lessons Learned
===============

Initially our group separated the work into parts to investigate the IntelliJ architecture. This did not help our understanding as it was difficult to piece together on how each part contributed to the overall architecture. Lack of official documentation makes finding information harder. Due to poor official documentation, most of the time we had to look at the code which is very time consuming. The pieces begin to fit together after our first group discussion and we found more group discussion should have happened earlier to reach a faster understanding into the architecture.

* * *

[\[1\]](#ftnt_ref1)[http://en.wikipedia.org/wiki/Inversion\_of\_control](https://www.google.com/url?q=http://en.wikipedia.org/wiki/Inversion_of_control&sa=D&ust=1570467390048000)

[\[2\]](#ftnt_ref2)[https://picocontainer.com/](https://www.google.com/url?q=https://picocontainer.com/&sa=D&ust=1570467390048000)

[\[3\]](#ftnt_ref3)[https://github.com/JetBrains/intellij-community/blob/4999f5293e4307870020f1d0d672a3d35a52f22d/lib/annotations/picocontainer/org/picocontainer/annotations.xml](https://www.google.com/url?q=https://github.com/JetBrains/intellij-community/blob/4999f5293e4307870020f1d0d672a3d35a52f22d/lib/annotations/picocontainer/org/picocontainer/annotations.xml&sa=D&ust=1570467390047000)

[\[4\]](#ftnt_ref4) http://gitstats.sourceforge.net/

[\[5\]](#ftnt_ref5) https://github.com/JetBrains/intellij-community

[\[6\]](#ftnt_ref6) http://aosabook.org/en/eclipse.html

[\[7\]](#ftnt_ref7) https://www.slideshare.net/intelliyole/intelli-j-platform